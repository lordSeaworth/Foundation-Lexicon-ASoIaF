# Lexicon: A Song of Ice and Fire

Simple name mod for Foundation  
Gives your new villagers names based on characters from the works of G.R.R Martin from his novels "A Song of Ice and Fire"

## Content tree

```
├── scripts
│   └── villagers
│       ├── female.lua
│       └── male.lua
├── generated_ids.lua
├── logo.png
├── mod.json
├── mod.lua
└── README.md
```